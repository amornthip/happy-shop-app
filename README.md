# Happy Shop application

This project is bootstrapped with [Create Elm App](https://github.com/halfzebra/create-elm-app).

## Installing Elm packages

```sh
elm-app install <package-name>
```

## Folder structure

```
my-app/
├── .gitignore
├── README.md
├── elm-package.json
├── elm-stuff
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo.svg
│   └── manifest.json
├── src
│   ├── Main.elm
│   ├── index.js
│   ├── main.css
│   └── registerServiceWorker.js
└── tests
    ├── Tests.elm
    └── elm-package.json
```

For the project to build, these files must exist with exact filenames:

- `public/index.html` is the page template;
- `public/favicon.ico` is the icon you see in the browser tab;
- `src/index.js` is the JavaScript entry point.

You can delete or rename the other files.

You may create subdirectories inside src.

## Available scripts
In the project directory you can run:
### `elm-app build`
Builds the app for production to the `build` folder.

The build is minified, and the filenames include the hashes.
Your app is ready to be deployed!

### `elm-app start`
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `elm-app install`

An alias for [`elm-app package install`](#package)

### `elm-app test`
Run tests with [node-test-runner](https://github.com/rtfeldman/node-test-runner/tree/master)

You can make test runner watch project files by running:
```sh
elm-app test --watch
```
module Main exposing (..)

import Html
import Model exposing (Product, Model, Msg(SelectCategoryFilter))
import Bootstrap.Dropdown as Dropdown
import Update exposing (getProducts, update)

import View exposing (view)

initialProducts: List Product
initialProducts = []


init : ( Model, Cmd Msg )
init =
    ( Model initialProducts Dropdown.initialState, getProducts )

---- SUBSCRIPTIONS ----

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Dropdown.subscriptions model.filterCategory SelectCategoryFilter ]

---- PROGRAM ----


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }

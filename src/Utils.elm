module Utils exposing (..)

import Model exposing (Product)
import Set

displayPrice : Int -> String
displayPrice price = 
  price
    |> toFloat
    |> toString
    |> String.append "$"


getCategories : List Product -> List String
getCategories products =
  products
    |> List.map (\p -> p.category)
    |> Set.fromList
    |> Set.toList